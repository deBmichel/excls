package generatealgorithmandword

import (
	"log"
    "os"
    "testing"
)

// use the next command to run the tests.
// go clean -testcache && go test ./... -v

// TestGenerator test if generator generate algorithm and secret word Ids.
func TestGeneratorAlgorithmAndSecretWord (t *testing.T) {
	log.Println("runing test TestGeneratorAlgorithmAndSecretWord")
    os.Setenv("NUMBER_OF_ALGORITHMS", "20")
    os.Setenv("NUMBER_OF_WORDS", "23")
	alg, sword := -1, -1
	alg, sword = GenerateAlgorithmAndWordIds()
	if alg == -1 || sword == -1 {
		t.Fatalf(`GenerateAlgorithmAndWordIds() is not working`)
	}
	log.Println("algorithm and word ids are :", alg, sword)
	os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
	log.Println("finishing test TestGeneratorAlgorithmAndSecretWord")
}
