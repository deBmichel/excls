package generatealgorithmandword

import (
	raasw "excls_clock/services/ramdomalgorithmandsecretword/common/structures"
)

// GenerateAlgorithmAndWordIds generate a random integer id for algorithm and secret word.
func GenerateAlgorithmAndWordIds()(int, int) {
	return raasw.G.GetUseGenrtr().ChoiseAlgorithm(), raasw.G.GetUseGenrtr().ChoiseSecretWord()
}
