package generatealgorithm

import (
	"log"
    "os"
    "testing"
)

// Use the next command to execute the tests:
// go clean -testcache && go test ./... -v

// TestAlgorithmGenerator test if generator generate algorithm id.
func TestAlgorithmGeneratorId (t *testing.T) {
	log.Println("runing test TestAlgorithmGeneratorId")
    os.Setenv("NUMBER_OF_ALGORITHMS", "20")
    os.Setenv("NUMBER_OF_WORDS", "23")
	alg := -1
	alg = GenerateAlgorithmId()
	if alg == -1 {
		t.Fatalf(`GenerateAlgorithmId() is not working`)
	}
	log.Println("algorithm id is :", alg)
	os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
	log.Println("finishing test TestAlgorithmGeneratorId")
}
