package generatealgorithm

import (
	raasw "excls_clock/services/ramdomalgorithmandsecretword/common/structures"
)

// GenerateAlgorithm generate a random integer id for algorithm, only algorithm.
func GenerateAlgorithmId()(int) {
	return raasw.G.GetUseGenrtr().ChoiseAlgorithm()
}