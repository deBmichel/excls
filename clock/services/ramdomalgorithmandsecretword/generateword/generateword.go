package generateword

import (
	raasw "excls_clock/services/ramdomalgorithmandsecretword/common/structures"
)

// GenerateSecretWordId() generate a random integer id for secretword, only secret word.
func GenerateSecretWordId()(int) {
	return raasw.G.GetUseGenrtr().ChoiseSecretWord()
}