package generateword

import (
	"log"
    "os"
    "testing"
)

// Use the next command to execute the tests:
// go clean -testcache && go test ./... -v

// TestGenerateSecretWordId test if generator generate Secretword id.
func TestGenerateSecretWordId (t *testing.T) {
	log.Println("runing test TestGenerateSecretWordId")
    os.Setenv("NUMBER_OF_ALGORITHMS", "20")
    os.Setenv("NUMBER_OF_WORDS", "23")
	secretword := -1
	secretword = GenerateSecretWordId()
	if secretword == -1 {
		t.Fatalf(`GenerateSecretWordId() is not working`)
	}
	log.Println("secretword Id is:", secretword)
	os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
	log.Println("finishing test TestGenerateSecretWordId")
}