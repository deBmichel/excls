package obtainalgwrdwthid

import (
	"log"
)

const (
	errorId = -1
	ServiceFail = "SERVICE_FAIL"
)

type Service struct{
	started bool
}

var Srvc *Service = &Service{false}

func (s *Service) verifyStart () bool {
	log.Println("Starting service obtainalgwrdwthid")
	if !verifyTable() {
		log.Println("Imposible start table from obtainalgwrdwthid !!")
		s.started = false
		return false
	} else {
		log.Println("Service obtainalgwrdwthid started")
		s.started = true
		return true
	}
}

func (s *Service) Obtainalgwrdwthid(id int) (bool, string, []int){
	if !s.started {
		if s.verifyStart() {
			return getAlgWordIdsUsingId(id)
		} else {
			log.Println("Error starting service for getting ids from obtainalgwrdwthid !!!")
			return false, ServiceFail, []int{errorId, errorId, errorId}
		}
	} else {
		return getAlgWordIdsUsingId(id)
	}
}