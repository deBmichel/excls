package obtainalgwrdwthid


import (
	"log"
    "testing"
	"os"

	creator "excls_clock/services/ramdomalgorithmandsecretword/generatealgwrdprsstnt"
)

// use the next command to run the tests.
// go clean -testcache && go test ./... -v

// TestGetIds test if the function to get algorithm and secret word Ids in db is functional.
func TestGetIds (t *testing.T) {
	log.Println("runing test TestGetIds")
	os.Setenv("USER_DB","postgres")
	os.Setenv("PASSWORD_DB","postgres")
	os.Setenv("NAME_DB","excls")
	os.Setenv("HOST_DB","0.0.0.0")
	os.Setenv("PORT_DB","5432")
	os.Setenv("NUMBER_OF_ALGORITHMS","10")
	os.Setenv("NUMBER_OF_WORDS","10")
	
	log.Println("Step 1: Generate Ids and save in db")
	flag, ids := creator.Srvc.GeneratePersistentAlgrthmAndWordId()
	if !flag {
		t.Fatalf(`GeneratePersistentAlgrthmAndWordId() is not working`)
	}

	id, algId, wordId := ids[0], ids[1], ids[2]
	if id == errorId || algId == errorId || wordId == errorId {
		t.Fatalf(`GeneratePersistentAlgrthmAndWordId() is not generating ids in a correct way !!!!`)
	}
	log.Println("Obtained ids:", id, algId, wordId)
	log.Println("Step 2: Get ids using the service function: Obtainalgwrdwthid")
	_, status, ids := Srvc.Obtainalgwrdwthid(id)
	if status != Aproved {
		t.Fatalf(`Obtainalgwrdwthid get status != aproved ; get = %v`, status)
	}

	if ids[0] != id {
		t.Fatalf(`Obtainalgwrdwthid failed with id; id = %v, expected = %v`, ids[0], id)
	}

	if ids[1] != algId {
		t.Fatalf(`Obtainalgwrdwthid failed with algId; id = %v, expected = %v`, ids[1], algId)
	}

	if ids[2] != wordId {
		t.Fatalf(`Obtainalgwrdwthid failed with wordId; id = %v, expected = %v`, ids[2], wordId)
	}

	os.Setenv("USER_DB","")
	os.Setenv("PASSWORD_DB","")
	os.Setenv("NAME_DB","")
	os.Setenv("HOST_DB","")
	os.Setenv("PORT_DB","")
	os.Setenv("NUMBER_OF_ALGORITHMS","")
	os.Setenv("NUMBER_OF_WORDS","")
	log.Println("finishing test TestGetIds")
}