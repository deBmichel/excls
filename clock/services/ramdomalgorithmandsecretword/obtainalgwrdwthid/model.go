package obtainalgwrdwthid

import (
	"log"
	"strconv"
	"strings"

	db "excls_clock/services/ramdomalgorithmandsecretword/common/db"
)

const (
	intError = -1
	ModelErrUnknow = "DB_ERROR_UNKNOWN_LOGIC"
	DbError = "DB_ERROR"
	NoData = "NO_DATA/DATA_ERROR/ID_WITH_NODATA"
	Aproved = "APROVED"

	Errorsubnorows = "no rows in result"
	Errorsubrefusc = "connection refused"
)


func verifyTable () bool {
	obtain, db := db.Db.GetUseDb()
	if !obtain {
		log.Println("Error obtaining db conector : verifyTable : from obtainalgwrdwthid !!")
		return false
	}

	tablegen := `
		CREATE TABLE IF NOT EXISTS algorithmwordids (
			id SERIAL PRIMARY KEY,
			algorithmid INT NOT NULL,
			wordid INT NOT NULL
		);
	`
	_, err := db.Exec(tablegen)
	if err != nil {
		log.Println("Error starting table from obtainalgwrdwthid !!", err)
		return false
	}

	log.Println("Table algorithmwordids was started ....")
	return true
}

func getErrorTypeClasified(err error) string {
	if strings.Contains(err.Error(), Errorsubnorows){
		return NoData
	}

	if strings.Contains(err.Error(), Errorsubrefusc){
		return DbError
	}

	return ModelErrUnknow
}

func getAlgWordIdsUsingId (idQuery int) (bool, string,[]int){
	ret := []int{intError, intError, intError}
	obtain, db := db.Db.GetUseDb()
	if !obtain {
		log.Println("Error obtaining db conector : verifyTable : from obtainalgwrdwthid !!")
		return false, DbError, ret
	}

	selQuery := `select id, algorithmid, wordid from algorithmwordids where id = `+strconv.Itoa(idQuery)+`;`
	ids := Idsgenerated{intError, intError, intError,}
	err := db.Get(&ids, selQuery)
	if err != nil {
		log.Println("Missing data or error ? in getAlgWordIdsUsingId from obtainalgwrdwthid:", err)
		flag := getErrorTypeClasified(err)
		if flag == DbError {
			return false, DbError, ret
		}

		if flag == NoData {
			return false, NoData, ret
		}

		if flag == ModelErrUnknow {
			log.Println("Unclasified Error", err)
			return false, ModelErrUnknow, ret
		}
	}

	if ids.CreateID == intError || ids.AlgorithmID == intError || ids.WordID== intError {
		log.Println("Obtained negative value !!! getAlgWordIdsUsingId from obtainalgwrdwthid")
		return false, NoData, []int{ids.CreateID, ids.AlgorithmID, ids.WordID}
	}

	return true, Aproved, []int{ids.CreateID, ids.AlgorithmID, ids.WordID}
}
