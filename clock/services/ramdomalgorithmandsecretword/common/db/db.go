package db

import (
    "errors"
	"log"
	"os"
	
    "github.com/jmoiron/sqlx"
    _ "github.com/lib/pq"
)

type Dbc struct {
	db *sqlx.DB
	isStarted bool
}

func (db *Dbc) GetUseDb () (bool, *sqlx.DB) {
	if db.isStarted {
		return true, db.db
	} else {
		if db.ConnectWithDb() {
			return true, db.db
		} else {
			log.Println("Down : bad connect with db from db")
			return false, db.db
		}
	}
}

func (db *Dbc) ConnectWithDb () bool {
	user, dbname := os.Getenv("USER_DB"), os.Getenv("NAME_DB") 
	password, host := os.Getenv("PASSWORD_DB"), os.Getenv("HOST_DB")
	port := os.Getenv("PORT_DB")
	return db.initDb(user, dbname, password, host, port)
}

func (db *Dbc) initDb(user, dbname, password, host, port string) bool {
	log.Println("Trying to connect to db from db !!")
	connectionstr :=` user=`+user+
					` dbname=`+dbname+
					` password=`+password+
					` host=`+host+
					` port=`+port+
					` sslmode=disable`
	
	err := errors.New("")
	db.db, err = sqlx.Connect("postgres", connectionstr)
	if err != nil {
		log.Println("Error connecting to db from db !!",err)
		db.isStarted = false
		return false
	}

  	// Test connection to database
  	if err := db.db.Ping(); err != nil {
		log.Println("DB ping failed from db !!", err)
		db.isStarted = false
		return false
  	} else {
		log.Println("Successfully Connected to DB from db")
		db.isStarted = true
		return true
  	}
}