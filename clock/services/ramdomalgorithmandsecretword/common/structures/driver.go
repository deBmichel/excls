package structures

type Gen struct {
	gen *Control
}

func (g *Gen) GetUseGenrtr() *Control {
	if g.gen.isStarted {
		return g.gen
	} else {
		g.gen.ReviewStart()
		return g.gen
	}
}

var C Control
var G Gen = Gen{&C}
// Use G.GetUseGenrtr()


