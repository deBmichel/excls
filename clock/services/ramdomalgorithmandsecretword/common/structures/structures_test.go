package structures

/*
    To run all the tests use the command:
    go clean -testcache && go test ./... -v
*/

import (
	"log"
    "os"
    "testing"
)

// TestStructStartMissingAllEnvVars calls Start() in Control struct, the test should give false,
// the reason: NUMBER_OF_ALGORITHMS and NUMBER_OF_WORDS are not configured.
func TestStructStartMissingAllEnvVars(t *testing.T) {
    log.Println("runing test TestStructStartMissingAllEnvVars")
    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
    var c Control
    wait, flag := false, c.Start()
    if flag == true {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }
    log.Println("finish test TestStructStartMissingAllEnvVars")
}

// TestStructStartMissingNumberOfAlgorithms calls Start() in Control struct, the test should give 
// false, the reason: NUMBER_OF_ALGORITHMS is not configured.
func TestStructStartMissingNumberOfAlgorithms(t *testing.T) {
    log.Println("runing test TestStructStartMissingNumberOfAlgorithms")
    os.Setenv("NUMBER_OF_WORDS", "20")
    var c Control
    wait, flag := false, c.Start()
    if flag == true {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }
    os.Setenv("NUMBER_OF_WORDS", "")
    log.Println("finish test TestStructStartMissingNumberOfAlgorithms")
}

// TestStructStartMissingNumberOfWords calls Start() in Control struct, the test should give 
// false, the reason: NUMBER_OF_WORDS is not configured.
func TestStructStartMissingNumberOfWords(t *testing.T) {
    log.Println("runing test TestStructStartMissingNumberOfWords")
    os.Setenv("NUMBER_OF_ALGORITHMS", "20")
    var c Control
    wait, flag := false, c.Start()
    if flag == true {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }
    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    log.Println("finish test TestStructStartMissingNumberOfWords")
}

// TestStructStart calls Start() in Control struct, the test should give true, the reason: 
// NUMBER_OF_WORDS and NUMBER_OF_ALGORITHMS ARE configured.
func TestStructStart(t *testing.T) {
    log.Println("runing test TestStructStart")
    os.Setenv("NUMBER_OF_ALGORITHMS", "20")
    os.Setenv("NUMBER_OF_WORDS", "23")

    var c Control
    wait, flag := false, c.Start()
    if flag == false {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }

    if c.isStarted == false {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }

    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
    log.Println("finish test TestStructStart")
}

// TestMinimalWordsAlgs try Start() Control Struct using value less than 3,
// in this case Start() should return false
func TestMinimalWordsAlgs (t *testing.T){
    log.Println("runing test TestMinimalWordsAlgs")
    os.Setenv("NUMBER_OF_ALGORITHMS", "2")
    os.Setenv("NUMBER_OF_WORDS", "2")
   
    var c Control
    wait, flag := false, c.Start()
    if flag == true {
        t.Fatalf(`c.Start() = %v, want match for %v`, flag, wait)
    }

    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
    log.Println("finish test TestMinimalWordsAlgs")
}

// TestChoiseAlgorithms: for this test we start correctly a Control struct, we assing 
// NUMBER_OF_ALGORITHMS and NUMBER_OF_WORDS, and we are looking for obtain n numbers of
// algorithms.
func TestChoiseAlgorithms (t *testing.T) {
    log.Println("runing test TestChoiseAlgorithms")
    nalgorithmsstr, expected := "5", 10
    os.Setenv("NUMBER_OF_ALGORITHMS", nalgorithmsstr)
    os.Setenv("NUMBER_OF_WORDS", "5")
    var c Control
    c.Start()

    var algorithms []int
    for i:=0; i<expected; i++ {
        algorithms = append(algorithms, c.ChoiseAlgorithm())
    }

    if len(algorithms) != expected {
        t.Fatalf(`len(algorithms) = %v, want match for %v`, len(algorithms), expected)
    }
    
    log.Println(algorithms)
    
    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
    log.Println("finish test TestChoiseAlgorithms")
}

// TestChoiseWords: for this test we start correctly a Control struct, we assing 
// NUMBER_OF_ALGORITHMS and NUMBER_OF_WORDS, and we are looking for obtain n numbers of
// words.
func TestChoiseWords (t *testing.T) {
    log.Println("runing test TestChoiseWords")
    nwordsstr, expected := "5", 24
    os.Setenv("NUMBER_OF_ALGORITHMS", "10")
    os.Setenv("NUMBER_OF_WORDS", nwordsstr)
    var c Control
    c.Start()

    var words []int
    for i:=0; i<expected; i++ {
        words = append(words, c.ChoiseSecretWord())
    }

    if len(words) != expected {
        t.Fatalf(`len(words) = %v, want match for %v`, len(words), expected)
    }
    
    log.Println(words)
    
    os.Setenv("NUMBER_OF_ALGORITHMS", "")
    os.Setenv("NUMBER_OF_WORDS", "")
    log.Println("finish test TestChoiseAlgorithms")
}

