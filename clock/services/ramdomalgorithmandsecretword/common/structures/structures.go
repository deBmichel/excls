package structures

import (
	"log"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"
)

const MinimalAlgsWords = 3

type Control struct {
	mu sync.Mutex
	aAlgorithms, bAlgorithms [] int
	aSecretWrds, bSecretWrds [] int
	isStarted bool
}

func (c *Control) Start () bool {
	numberOfAlgorithms, err := strconv.Atoi(os.Getenv("NUMBER_OF_ALGORITHMS"))
    if err != nil {
		log.Println("NUMBER_OF_ALGORITHMS was not configured !!!!")
		return false
    }

	numberOfWords, err := strconv.Atoi(os.Getenv("NUMBER_OF_WORDS"))
    if err != nil {
		log.Println("NUMBER_OF_WORDS was not configured !!!!")
		return false
    }

	if numberOfAlgorithms < MinimalAlgsWords || numberOfWords < MinimalAlgsWords {
		log.Println("NUMBER_OF_ALGORITHMS OR NUMBER_OF_WORDS IS LOW")
		return false
	}


	c.aAlgorithms = c.ChargeSliceToChoiceProcess(numberOfAlgorithms)
	c.aSecretWrds = c.ChargeSliceToChoiceProcess(numberOfWords)
	c.isStarted = true

	return true
}


func (c *Control) ChargeSliceToChoiceProcess(amount int) []int {
	slice := make([]int, amount)
	for i:=0; i<len(slice); i+=1 {
		slice[i] = i
	}
	return slice
}

func (c *Control) ChoiceUsingTwoListsFromAToB (a, b *[]int) int {
	if len(*a) != 1 {
		// Choise element in a
		lenA, source := len(*a), rand.NewSource(time.Now().UnixNano())
		generator := rand.New(source)
  		indxElemnt := generator.Intn(lenA)
		element := (*a)[indxElemnt]
		// Delete element in a
		copy((*a)[indxElemnt:], (*a)[indxElemnt+1:])
    	*a = (*a)[:lenA-1]
		// Adding element in b
		*b = append(*b, element)
		return element
	} else {
		element := (*a)[0]
		*b = append(*b, element)
		*a = *b
		*b = []int{}
		return element
	}
}

func (c *Control) ChoiseAlgorithm () int {
	// Locking access to slices: secure updating and choosing.
	c.mu.Lock()
	
	algorithm := c.ChoiceUsingTwoListsFromAToB(&c.aAlgorithms, &c.bAlgorithms)
		
	// Unlocking access to slices.
	c.mu.Unlock()
	return algorithm
}

func (c *Control) ChoiseSecretWord() int {
	// Locking access to slices: secure updating and choosing.
	c.mu.Lock()
	
	word := c.ChoiceUsingTwoListsFromAToB(&c.aSecretWrds, &c.bSecretWrds)
		
	// Unlocking access to slices.
	c.mu.Unlock()
	return word
}

func (c *Control) ReviewStart(){
	if !c.isStarted {
		log.Println("Trying start control struct.....")
		if !c.Start() {
			log.Fatal("Imposible init generator in ramdomalgorithmandsecretword common!!!!")
		} else {
			log.Println("ramdomalgorithmandsecretword COMMON Generator was started")
		}	
	} 
}


