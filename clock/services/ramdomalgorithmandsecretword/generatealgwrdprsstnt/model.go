package generatealgwrdprsstnt

import (
	"log"

	db "excls_clock/services/ramdomalgorithmandsecretword/common/db"
)

func startTable () bool {
	obtain, db := db.Db.GetUseDb()
	if !obtain {
		log.Println("Error obtaining db conector : startTable : from generatealgwrdprsstnt !!")
		return false
	}

	tablegen := `
		CREATE TABLE IF NOT EXISTS algorithmwordids (
			id SERIAL PRIMARY KEY,
			algorithmid INT NOT NULL,
			wordid INT NOT NULL
		);
	`
	_, err := db.Exec(tablegen)
	if err != nil {
		log.Println("Error starting table from generatealgwrdprsstnt !!", err)
		return false
	}

	log.Println("Table algorithmwordids was started ....")
	return true
}

func saveAlgorithmAndWordIds (algId, wordId int) (bool, int) {
	obtain, db:= db.Db.GetUseDb()
	ret, id := false, -1
	if !obtain {
		log.Println("Error obtaining db conector : saveAlgorithmAndWordIds : from generatealgwrdprsstnt !!")
		return ret, id
	}
	
	tdx, err := db.Begin()
	if err != nil {
		log.Println("Error in db.Begin() saveAlgorithmAndWordIds generatealgwrdprsstnt !!! : ", err)
		return ret, id
	}

	stmt, err := tdx.Prepare(`
		INSERT INTO algorithmwordids (
			algorithmid, wordid
		) VALUES ($1, $2) 
		RETURNING id;
	`)
	if err != nil {
		log.Println("Error in tx.Prepare( saveAlgorithmAndWordIds generatealgwrdprsstnt !!! : ", err)
		return ret, id
	}
	defer stmt.Close()

	err = stmt.QueryRow(algId,wordId,).Scan(&id)
	if err != nil {
		log.Println("Error in stmt.QueryRow( saveAlgorithmAndWordIds generatealgwrdprsstnt !!! : ", err)
		return ret, id
	}
	
	err = tdx.Commit()
	if err != nil {
		log.Println("Error in tx.commit( saveAlgorithmAndWordIds generatealgwrdprsstnt !!! : ", err)
		return ret, id
	}
	
	return true, id
}
