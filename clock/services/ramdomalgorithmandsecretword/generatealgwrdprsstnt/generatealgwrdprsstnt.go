package generatealgwrdprsstnt

import (
	"log"

	raasw "excls_clock/services/ramdomalgorithmandsecretword/common/structures"
)

const (
	errorId = -1
)

type Service struct{
	started bool
}

var Srvc *Service = &Service{false}

func (s *Service) verifyStart () bool {
	log.Println("Starting service generatealgwrdprsstnt")
	if !startTable() {
		log.Println("Imposible start table from generatealgwrdprsstnt !!")
		s.started = false
		return false
	} else {
		log.Println("Service generatealgwrdprsstnt started")
		s.started = true
		return true
	}
}

func (s *Service) createAlgorithmWordIds() (bool, []int){
	algId, wordId := raasw.G.GetUseGenrtr().ChoiseAlgorithm(), raasw.G.GetUseGenrtr().ChoiseSecretWord()
	saved, id := saveAlgorithmAndWordIds (algId, wordId)
	if saved {
		return true, []int{id, algId, wordId}
	} else {
		log.Println("Failed saving id from generatealgwrdprsstnt !!!")
		return false, []int{errorId, errorId, errorId}
	}
}

func (s *Service) GeneratePersistentAlgrthmAndWordId() (bool, []int){
	if !s.started {
		if s.verifyStart() {
			return s.createAlgorithmWordIds()
		} else {
			log.Println("Error starting service for saving ids from generatealgwrdprsstnt !!!")
			return false, []int{errorId, errorId, errorId}
		}
	} else {
		return s.createAlgorithmWordIds()
	}
}

