package generatealgwrdprsstnt

import (
	"log"
    "testing"
	"os"
)

// use the next command to run the tests.
// go clean -testcache && go test ./... -v

// TestPersistentGenerator test if generator generate algorithm and secret word Ids, saving in db.
func TestPersistentGenerator (t *testing.T) {
	log.Println("runing test TestPersistentGenerator")
	os.Setenv("USER_DB","postgres")
	os.Setenv("PASSWORD_DB","postgres")
	os.Setenv("NAME_DB","excls")
	os.Setenv("HOST_DB","0.0.0.0")
	os.Setenv("PORT_DB","5432")
	os.Setenv("NUMBER_OF_ALGORITHMS","10")
	os.Setenv("NUMBER_OF_WORDS","10")
	
	flag, ids := Srvc.GeneratePersistentAlgrthmAndWordId()
	if !flag {
		t.Fatalf(`GeneratePersistentAlgrthmAndWordId() is not working`)
	}

	id, algId, wordId := ids[0], ids[1], ids[2]
	if id == errorId || algId == errorId || wordId == errorId {
		t.Fatalf(`GeneratePersistentAlgrthmAndWordId() is not generating ids in a correct way !!!!`)
	}
	log.Println("Obtained ids:", id, algId, wordId)

	os.Setenv("USER_DB","")
	os.Setenv("PASSWORD_DB","")
	os.Setenv("NAME_DB","")
	os.Setenv("HOST_DB","")
	os.Setenv("PORT_DB","")
	os.Setenv("NUMBER_OF_ALGORITHMS","")
	os.Setenv("NUMBER_OF_WORDS","")
	log.Println("finishing test TestPersistentGenerator")
}