package datetime

import (
	"fmt"
	"time"
)

func GetTimeInServerMachine () time.Time {
	return time.Now()
}

func GetFormatedHourandDateInServer () string {
	tInSrvr := GetTimeInServerMachine()
	formated := fmt.Sprintf(
		"date (Year-Month-Day)= %v-%v-%v ; hour (Hour:Minute:Second)= %v:%v:%v",
		tInSrvr.Year(), tInSrvr.Month(), tInSrvr.Day(), 
		tInSrvr.Hour(), tInSrvr.Minute(), tInSrvr.Second(),
	)
	return formated
}