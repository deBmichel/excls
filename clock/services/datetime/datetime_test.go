package datetime

import (
	"log"
    "testing"
	"regexp"
)

// to run tests use the command:
// go clean -testcache && go test ./... -v

// TestDate test if GetHourAndDateInServer() return a string similar to a date, here
// the validation is using regex.
func TestDate (t *testing.T) {
	log.Println("Runing test TestDate")
	datehour := GetHourAndDateInServer()
	matched, err := regexp.Match(`[\d{2}:\d{2}:\d{2}]`, 
	[]byte(datehour))
	if err != nil {
		log.Fatalf("serious error in TestDate test review the code !!!")
	}

	log.Println("obtained hour and date : ", datehour)
	log.Println("Todo: change the REGEX AND VALIDATE CORRECTLY !!!!!!!!!!")

	if !matched {
		log.Fatalf("Problems obtainig date and hour vía GetHourAndDateInServer()")
	}

	log.Println("Finishing test TestDate")
}