package datetime

// GetHourAndDateInServer return hour in date in server, in form: 
// date (Year-Month-Day)= 2023-December-22 ; hour (Hour:Minute:Second)= 12:26:12
func GetHourAndDateInServer() string {
	return GetFormatedHourandDateInServer ()
}