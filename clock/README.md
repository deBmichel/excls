The clock:

The clock generates randoms ids numbers for algorithms and words depending of 
the values NUMBER_OF_ALGORITHMS and NUMBER_OF_WORDS configured.

Five services were written and can be consumed using gingonic or fasthttp(with 
phi router).

Api Gingonic
    GET    http://localhost:3210/units/prsstntalgwrdIds   
    GET    http://localhost:3210/units/obtainalgwrdwthid/(IdNUmber) 
    GET    http://localhost:3210/gnrtrs/algorithmIdgenerator 
    GET    http://localhost:3210/gnrtrs/algandwrdIdsgenrtr
    GET    http://localhost:3210/gnrtrs/wordIdgenerator 


Api Fasthttp
    GET    http://localhost:4321/units/prsstntalgwrdIds   
    GET    http://localhost:4321/units/obtainalgwrdwthid/?id=(IdNUmber)  
    GET    http://localhost:4321/gnrtrs/algorithmIdgenerator  
    GET    http://localhost:4321/gnrtrs/algandwrdIdsgenrtr 
    GET    http://localhost:4321/gnrtrs/wordIdgenerator






