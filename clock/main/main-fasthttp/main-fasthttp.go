package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	
	runner "excls_clock/exposers/fasthttpexp"
)

func main () {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	log.Println(os.Getenv("GREETING"))
	
	var r runner.RouterRunner
	r.ExecuteFast()
	
}
