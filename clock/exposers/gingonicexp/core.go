package gingonicexp

import (
	"log"
	"os"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	
	groups "excls_clock/exposers/gingonicexp/exhibitions/groups"
	units "excls_clock/exposers/gingonicexp/exhibitions/units"
)

const (
	timelive = 12
)

func GetValidOrigin() string {
	return os.Getenv("ALLOWEDORIGIN")
}

func config() cors.Config {
	return cors.Config{
		AllowOrigins: []string{
			GetValidOrigin(),
		},
		AllowMethods: []string{
			"POST", "GET", "OPTIONS", "DELETE", "PATCH",
		},
		ExposeHeaders: []string{
			"Content-Length", "Content-Type", "Access-Control-Allow-Methods",
		},
		AllowHeaders: []string{
			"Accept", "Access-Control-Allow-Origin", "Accept-Encoding", "Authorization",
			"Content-Type", "Content-Length", "Host", "Origin", "Referrer", "User-Agent",
		},
		AllowOriginFunc: func(origin string) bool {
			return origin == GetValidOrigin()
		},
		MaxAge:                 timelive * time.Hour,
		AllowAllOrigins:        false,
		AllowWildcard:          false,
		AllowBrowserExtensions: false,
		AllowWebSockets:  false,
		AllowFiles:       false,
		AllowCredentials: true,
	}
}

type RouterRunner struct {
	Router *gin.Engine
}

func (r *RouterRunner) Prepare () {
	r.Router = gin.Default()
	r.Router.ForwardedByClientIP = true
	r.Router.SetTrustedProxies([]string{"127.0.0.1", "192.168.0.1", "0.0.0.0"})
	r.Router.Use(cors.New(config()))
}

func (r *RouterRunner) GetGinPort () string {
	port := os.Getenv("GIN_PORT")
	if port == "" {
		log.Println("GIN_PORT was not configured asume 8080")
		port = "8080"
	}
	return port
}

func (r *RouterRunner) Execute () {
	units.WSGUnits(r.Router)
	groups.WSGAlalgwrdgnrtrs(r.Router)
	port := r.GetGinPort()
	log.Println("Posible direction :", "http://localhost:"+port)
	if err := r.Router.Run(":"+port); err != nil {
		log.Println("Error on running main runner ", err)
		panic(err)
	}
}

