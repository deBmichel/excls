package groups

import (
	"github.com/gin-gonic/gin"

	genalg "excls_clock/exposers/gingonicexp/exhibitions/services/generatealgorithm"
	genalgwrd "excls_clock/exposers/gingonicexp/exhibitions/services/generatealgorithmandword"
	genwrd "excls_clock/exposers/gingonicexp/exhibitions/services/generateword"
)

// WSAlgorithmGenerator define the service group /algwrdgnrtrs with the services:
// /wordIdgenerator, /algorithmandwordIdsgenerator, /algorithmIdgenerator
func WSGAlalgwrdgnrtrs(route *gin.Engine) {
	algeninrouter := route.Group("gnrtrs")
	algeninrouter.GET(genalg.Wsid, genalg.GetAlgorithmId)
	algeninrouter.GET(genalgwrd.Wsid, genalgwrd.GetAlgorithmAndWordIds)
	algeninrouter.GET(genwrd.Wsid, genwrd.GetWordId)
}
