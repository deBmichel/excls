package units

import (
	"github.com/gin-gonic/gin"

	gnprsstntalg "excls_clock/exposers/gingonicexp/exhibitions/services/generatealgwrdprsstnt"
	getAlgWrdIdsDb "excls_clock/exposers/gingonicexp/exhibitions/services/obtainalgwrdwthid"
)

const (
	WSGroup = "units" 
)

// WSGUnits define indvidual services in group /units:
func WSGUnits(route *gin.Engine) {
	algeninrouter := route.Group(WSGroup)
	algeninrouter.GET(gnprsstntalg.Wsid, gnprsstntalg.GetPersistentAlgrthmAndWordId)
	algeninrouter.GET(getAlgWrdIdsDb.Wsid, getAlgWrdIdsDb.GetAlgrthmAndWordIdInDb)
}