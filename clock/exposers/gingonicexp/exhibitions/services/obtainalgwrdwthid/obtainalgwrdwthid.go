package obtainalgwrdwthid

import (
	"net/http"
	"strconv"
	
	"github.com/gin-gonic/gin"

	algen "excls_clock/services/ramdomalgorithmandsecretword/obtainalgwrdwthid"
)

const (
	Wsid   = "/obtainalgwrdwthid/:id"
	Cmmnerrrws = "/obtainalgwrdwthid/:id failed, contact development team ...."
)

func validateIdlikeInt (id string) (bool, int) {
	isid , err := strconv.Atoi(id)
	if err != nil {
		return false, isid
	} else {
		return true, isid
	}
}

func GetAlgrthmAndWordIdInDb(c *gin.Context){
	validate, oid := validateIdlikeInt(c.Param("id"))
	if validate {
		obtained, status, ids := algen.Srvc.Obtainalgwrdwthid(oid)
		if obtained {
			IdCreated, AlgId, WordId := ids[0], ids[1], ids[2]
			c.JSON(http.StatusOK, gin.H{
				"IdCreation": IdCreated,
				"AlgorithmId": AlgId,
				"WordId": WordId,
			})
		} else {
			if status == algen.NoData {
				c.JSON(http.StatusOK, gin.H{
					"Message": "Service with no data for obtained Id or service failed...",
					"obtained Id ?": oid,
					"Obtained Data ?": ids,
				})
			}

			if status == algen.ModelErrUnknow {
				c.JSON(http.StatusInternalServerError, gin.H{
					"Message": "Service failed ? contact development team !!...",
					"obtained Id ?": oid,
					"Obtained Data ?": ids,
					"Obtained status": status,
				})
			}

			if status == algen.DbError {
				c.JSON(http.StatusInternalServerError, gin.H{
					"Message": Cmmnerrrws,
					"Info":algen.DbError,
					"Type?": "Internal error in db",
				})
			}

			if status == algen.ServiceFail{
				c.JSON(http.StatusInternalServerError, gin.H{
					"Message": Cmmnerrrws,
					"Info":algen.ServiceFail,
					"Type?": "Service Fail, service is not starting ??",
				})
			}
		}
	} else {
		c.JSON(http.StatusOK, gin.H{
			"Answer": "Id received is not a integer number",
			"Received Id ?": c.Param("id"),
		})
	}	
}
