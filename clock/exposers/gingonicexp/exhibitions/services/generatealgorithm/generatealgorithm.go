package generatealgorithm

import (
	"net/http"
	
	"github.com/gin-gonic/gin"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgorithm"
)

const (
	Wsid   = "/algorithmIdgenerator"
	Cmmnerrrws = "/algorithmIdgenerator failed, contact development team ...."
)

func GetAlgorithmId(c *gin.Context){
	algorithmId := algen.GenerateAlgorithmId()
	c.JSON(http.StatusOK, gin.H{
		"algorithmId": algorithmId,
	})
}



