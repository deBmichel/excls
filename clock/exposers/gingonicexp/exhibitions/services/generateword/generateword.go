package generateword

import (
	"net/http"
	
	"github.com/gin-gonic/gin"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generateword"
)

const (
	Wsid   = "/wordIdgenerator"
	Cmmnerrrws = "/wordIdgenerator failed, contact development team ...."
)

func GetWordId(c *gin.Context){
	wordId := algen.GenerateSecretWordId()
	c.JSON(http.StatusOK, gin.H{
		"wordId": wordId,
	})
}



