package generatealgorithmandword

import (
	"net/http"

	"github.com/gin-gonic/gin"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgorithmandword"
)

const (
	Wsid   = "/algandwrdIdsgenrtr"
	Cmmnerrrws = "/algandwrdIdsgenrtr failed, contact development team ...."
)

func GetAlgorithmAndWordIds(c *gin.Context){
	algorithmId, wordId := algen.GenerateAlgorithmAndWordIds()
	c.JSON(http.StatusOK, gin.H{
		"algorithmId": algorithmId,
		"wordId": wordId,
	})
}

