package generatealgwrdprsstnt

import (
	"net/http"
	
	"github.com/gin-gonic/gin"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgwrdprsstnt"
)

const (
	Wsid   = "/prsstntalgwrdIds"
	Cmmnerrrws = "/prsstntalgwrdIds failed, contact development team ...."
)

func GetPersistentAlgrthmAndWordId(c *gin.Context){
	saved, ids := algen.Srvc.GeneratePersistentAlgrthmAndWordId()
	if saved {
		SelId, AlgId, WordId := ids[0], ids[1], ids[2]
		c.JSON(http.StatusOK, gin.H{
			"IdCreation": SelId,
			"AlgorithmId": AlgId,
			"wordId": WordId,
		})
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Message": Cmmnerrrws,
		})
	}
	
}

