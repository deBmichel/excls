package fasthttpexp

import (
	"log"
	"os"
	"time"

	"github.com/fate-lovely/phi"
	"github.com/valyala/fasthttp"
	
	groups "excls_clock/exposers/fasthttpexp/exhibitions/groups"
	units "excls_clock/exposers/fasthttpexp/exhibitions/units"
)

type RouterRunner struct {}

func (r *RouterRunner) GetFastHttpPort () string {
	port := os.Getenv("FASTHTTP_PORT")
	if port == "" {
		log.Println("FASTHTTP_PORT was not configured asume 7789")
		port = "7789"
	}
	return port
}

func (r *RouterRunner) ExecuteFast () {
	x := phi.NewRouter()
	units.WSGUnits(x)
	groups.WSGAlalgwrdgnrtrs(x)
	server := &fasthttp.Server{
		Handler:     x.ServeFastHTTP,
		ReadTimeout: 10 * time.Second,
	}
	port := r.GetFastHttpPort()
	log.Println("Running in port :", port)
	log.Println("Posible direction :", "http://localhost:"+port)
	log.Fatal(server.ListenAndServe(":"+port))
}



