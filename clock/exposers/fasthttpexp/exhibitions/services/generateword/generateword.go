package generateword

import (
	"encoding/json"
		
	"github.com/valyala/fasthttp"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generateword"
)

const (
	Wsid   = "/wordIdgenerator"
	Cmmnerrrws = "/wordIdgenerator failed, contact development team ...."
)

type AlgIdJsoner struct {
	WordId int
}

func GetWordId(c *fasthttp.RequestCtx){
	strContentType := []byte("Content-Type")
	strApplicationJSON := []byte("application/json")
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	c.Response.SetStatusCode(200)
	wordId := algen.GenerateSecretWordId()
	obj := AlgIdJsoner {wordId}
	if err := json.NewEncoder(c).Encode(obj); err != nil {	
		c.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}



