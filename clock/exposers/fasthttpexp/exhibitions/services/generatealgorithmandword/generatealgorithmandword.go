package generatealgorithmandword

import (
	"encoding/json"
		
	"github.com/valyala/fasthttp"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgorithmandword"
)

const (
	Wsid   = "/algandwrdIdsgenrtr"
	Cmmnerrrws = "/algandwrdIdsgenrtr failed, contact development team ...."
)

type AlgIdJsoner struct {
	AlgId  int
	WordId int
}

func GetAlgorithmId(c *fasthttp.RequestCtx){
	strContentType := []byte("Content-Type")
	strApplicationJSON := []byte("application/json")
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	c.Response.SetStatusCode(200)
	algorithmId, wordId := algen.GenerateAlgorithmAndWordIds()
	obj := AlgIdJsoner {algorithmId, wordId}
	if err := json.NewEncoder(c).Encode(obj); err != nil {	
		c.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

