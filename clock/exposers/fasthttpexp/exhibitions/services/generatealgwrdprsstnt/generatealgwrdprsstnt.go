package generatealgwrdprsstnt

import (
	"encoding/json"
		
	"github.com/valyala/fasthttp"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgwrdprsstnt"
)

const (
	Wsid   = "/prsstntalgwrdIds"
	Cmmnerrrws = "/prsstntalgwrdIds failed, contact development team ...."
)

type AlgIdJsoner struct {
	IdCreation  int
	AlgorithmId int
	WordId      int
}

type AlgIdJsonerMssgErr struct {
	Message string
}

func GetPersistentAlgrthmAndWordId(c *fasthttp.RequestCtx){
	strContentType := []byte("Content-Type")
	strApplicationJSON := []byte("application/json")
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	c.Response.SetStatusCode(200)
	saved, ids := algen.Srvc.GeneratePersistentAlgrthmAndWordId()
	if saved {
		obj := AlgIdJsoner {ids[0], ids[1], ids[2]}
		if err := json.NewEncoder(c).Encode(obj); err != nil {	
			c.Error(err.Error(), fasthttp.StatusInternalServerError)
		}
	} else {
		obj := AlgIdJsonerMssgErr {Cmmnerrrws}
		if err := json.NewEncoder(c).Encode(obj); err != nil {	
			c.Error(err.Error(), fasthttp.StatusInternalServerError)
		}
	}
}
