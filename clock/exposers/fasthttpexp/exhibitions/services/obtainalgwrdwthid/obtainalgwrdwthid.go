package obtainalgwrdwthid

import (
	"encoding/json"
	"strconv"
	
	"github.com/valyala/fasthttp"

	algen "excls_clock/services/ramdomalgorithmandsecretword/obtainalgwrdwthid"
)

const (
	Wsid   = "/obtainalgwrdwthid/"
	Cmmnerrrws = "/obtainalgwrdwthid/ failed, contact development team ...."
)

func validateIdlikeInt (id string) (bool, int) {
	isid , err := strconv.Atoi(id)
	if err != nil {
		return false, isid
	} else {
		return true, isid
	}
}

type AlgIdJsoner struct {
	IdCreation   int
	AlgorithmId  int
	WordId       int
}

type MesssageInfoType struct {
    Message  string `json:"Message"`
    Info     string `json:"Info"`
    Type     string `json:"Type"`
}

type MesssageInfoDataType struct {
    Message  string `json:"Message"`
    Id       string `json:"obtained Id ?"`
    Data     []int  `json:"Obtained Data ?"`
	Status   string `json:"obtained Status"`
}

func WriteJSON(ctx *fasthttp.RequestCtx, code int, obj interface{}) {
	strContentType := []byte("Content-Type")
	strApplicationJSON := []byte("application/json")
	ctx.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	ctx.Response.SetStatusCode(code)
	if err := json.NewEncoder(ctx).Encode(obj); err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

func GetAlgrthmAndWordIdInDb(c *fasthttp.RequestCtx){
	soid := string(c.QueryArgs().Peek("id"))
	validate, oid := validateIdlikeInt(soid)
	if validate {
		obtained, status, ids := algen.Srvc.Obtainalgwrdwthid(oid)
		if obtained {
			obj := AlgIdJsoner {ids[0], ids[1], ids[2]}
			WriteJSON(c, 200, obj)
		} else {
			switch status {
			case algen.NoData:
				obj := MesssageInfoDataType{
					"Service with no data for obtained Id or service failed...",
					soid, ids, algen.NoData,
				}
				WriteJSON(c, 200, obj)
								
			case algen.ModelErrUnknow:
				obj := MesssageInfoDataType{
					"Service failed ? contact development team !!...",
					soid, ids, algen.ModelErrUnknow,
				}
				WriteJSON(c, 500, obj)
			case algen.DbError:
				obj := MesssageInfoType {Cmmnerrrws, algen.DbError, "Internal error in db"}
				WriteJSON(c, 500, obj)
			case algen.ServiceFail:
				obj := MesssageInfoType {Cmmnerrrws, algen.ServiceFail, "Service Fail, service is not starting ??"}
				WriteJSON(c, 500, obj)
			default:
				obj := MesssageInfoType {Cmmnerrrws, status, "Service Fail, MISSING LOGIC, contact development team.s"}
				WriteJSON(c, 500, obj)
			}
		}

	} else {
		msg :=  json.RawMessage(`
		{
			"Answer": "Id received is not a integer number",
			"Received Id ?": "`+soid+`"
		}`)
		WriteJSON(c, 200, msg)
	}
}
