package generatealgorithm

import (
	"encoding/json"
		
	"github.com/valyala/fasthttp"

	algen "excls_clock/services/ramdomalgorithmandsecretword/generatealgorithm"
)

const (
	Wsid   = "/algorithmIdgenerator"
	Cmmnerrrws = "/algorithmIdgenerator failed, contact development team ...."
)

type AlgIdJsoner struct {
	AlgId int
}

func GetAlgorithmId(c *fasthttp.RequestCtx){
	strContentType := []byte("Content-Type")
	strApplicationJSON := []byte("application/json")
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	c.Response.SetStatusCode(200)
	algorithmId := algen.GenerateAlgorithmId()
	obj := AlgIdJsoner {algorithmId}
	if err := json.NewEncoder(c).Encode(obj); err != nil {	
		c.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}





