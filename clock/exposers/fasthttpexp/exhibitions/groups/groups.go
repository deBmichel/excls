package groups

import (
	"github.com/fate-lovely/phi"
	"github.com/valyala/fasthttp"

	genalg "excls_clock/exposers/fasthttpexp/exhibitions/services/generatealgorithm"
	genalgwrd "excls_clock/exposers/fasthttpexp/exhibitions/services/generatealgorithmandword"
	genwrd "excls_clock/exposers/fasthttpexp/exhibitions/services/generateword"
)

// WSAlgorithmGenerator define the service group /algwrdgnrtrs with the services:
// /wordIdgenerator, /algorithmandwordIdsgenerator, /algorithmIdgenerator
func WSGAlalgwrdgnrtrs(router phi.Router) {
	groupname := "/gnrtrs"
	router.Group(func(r phi.Router) {
		r.Get(groupname+genalg.Wsid, func(ctx *fasthttp.RequestCtx) {
			genalg.GetAlgorithmId(ctx)
		})

		r.Get(groupname+genalgwrd.Wsid, func(ctx *fasthttp.RequestCtx) {
			genalgwrd.GetAlgorithmId(ctx)
		})

		r.Get(groupname+genwrd.Wsid, func(ctx *fasthttp.RequestCtx) {
			genwrd.GetWordId(ctx)
		})
	})
}



