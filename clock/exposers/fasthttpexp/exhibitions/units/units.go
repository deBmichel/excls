package units

import (
	"github.com/fate-lovely/phi"
	"github.com/valyala/fasthttp"

	gnprsstntalg "excls_clock/exposers/fasthttpexp/exhibitions/services/generatealgwrdprsstnt"
	getAlgWrdIdsDb "excls_clock/exposers/fasthttpexp/exhibitions/services/obtainalgwrdwthid"
)

const (
	WSGroup = "/units" 
)

// WSGUnits define indvidual services in group /units:
func WSGUnits(router phi.Router) {
	router.Group(func(r phi.Router) {
		r.Get(WSGroup+gnprsstntalg.Wsid, func(ctx *fasthttp.RequestCtx) {
			gnprsstntalg.GetPersistentAlgrthmAndWordId(ctx)
		})

		r.Get(WSGroup+getAlgWrdIdsDb.Wsid, func(ctx *fasthttp.RequestCtx) {
			getAlgWrdIdsDb.GetAlgrthmAndWordIdInDb(ctx)
		})
	})
}


